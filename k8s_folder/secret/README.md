# Create Secret in k8s with nexus and docker

## nexus and docker are the same

## 1st way

1. Create file name config.json or you can follow your name

```bash
{
 "auths": {
   "registry.e-crops.co": {    #follow your registry in nexus
     "username": "dara",
     "password": "123",
     "email": "dara@gmail.com",
     "auth": "ZGFyYToxMjM="    # echo -n "username:password" | base64
   }
 }
}

```
2.  You have to run base64 -w 0 config.json
```bash
base64 -w 0 config.json
```
!make sure you have to in location that you want to encrypt
![alt text](image/config.png)
copy the result and paste in secret.yml 

2. create file name secret.yml
```
apiVersion: v1
kind: Secret
metadata:
 name: registry-secret   #name for call to use 
type: kubernetes.io/dockerconfigjson
data:
 .dockerconfigjson: >-
ewogICJhdXRocyI6IHsKICAgICJyZWdpc3RyeS5lLWNyb3BzLmNvIjogewogICAgICAidXNlcm5hbWUiOiAiZGFyYSIsCiAgICAgICJwYXNzd29yZCI6ICIxMjMiLAogICAgICAiZW1haWwiOiAiZGFyYUBnbWFpbC5jb20iLAogICAgICAiYXV0aCI6ICJaR0Z5WVRveE1qTT0iCiAgICB9CiAgfQp9Cgo=

```
3.  Run command kubectl apply -f your file name
```bash
kubectl apply -f secret.yml
```

##  2nd way
1.  This one is for create secret too
```bash
kubectl create secret docker-registry my-dockerhub-secret \
  --docker-username=your username dockerhub \
  --docker-password=your access token dockerhub \
  --docker-email=you email dockerhub
```
if you want to see your secret name 
```bash
kubectl get secrets
```
![alt text](image/secret.png)
here is the example that we call name secret to use
```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nextjs-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nextjs-app
  template:
    metadata:
      labels:
        app: nextjs-app
    spec:
      imagePullSecrets:
      - name: my-dockerhub-secret   #follow your secret name
      containers:
      - name: nextjs
        image: muyleangin/nextjs:9
        ports:
        - containerPort: 3000

```